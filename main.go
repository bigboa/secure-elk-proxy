package main

import (
	"gitlab.com/NikolayZhurbin/secure-elk-proxy/server"
	"log"
)

func main() {
	err := server.StartServer()
	if err != nil {
		log.Fatalf("Server failed to start: %v", err)
	}
}
