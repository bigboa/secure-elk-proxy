package server

import (
	"context"
	"github.com/coreos/go-oidc"
	"log"
)

var (
	ClientID     string
	ClientSecret string
	RedirectURL  string
	Provider     *oidc.Provider

	Ctx context.Context

	err error
)

const (
	ElkUrl          = "http://46.17.45.170:9200/_search"
	DefaultBodyLean = 1000000
)

func init() {
	log.SetFlags(log.Lshortfile | log.LstdFlags)

	configURL := "http://46.17.45.170:8080/auth/realms/university-project"
	Ctx = context.Background()
	Provider, err = oidc.NewProvider(Ctx, configURL)
	if err != nil {
		panic(err)
	}

	ClientID = "ui-service"
	ClientSecret = "7354bbd7-9fc4-4693-a9af-43a29c731bae"
	RedirectURL = "http://localhost:8181/demo/callback"
}
