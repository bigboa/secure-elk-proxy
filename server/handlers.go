package server

import (
	"github.com/coreos/go-oidc"
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"
)

var (
	bodyLen int
	reqBody []byte
)

func Root(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		GetRoot(w, r)
	default:
		GetRoot(w, r)
	}
}

func GetRoot(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("content-type", "application/json; charset=UTF-8")

	oidcConfig := &oidc.Config{
		ClientID: ClientID,
	}
	verifier := Provider.Verifier(oidcConfig)
	rawAccessToken := r.Header.Get("Authorization")
	if rawAccessToken == "" {
		log.Println("rawAccessToken is empty")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	parts := strings.Split(rawAccessToken, " ")
	if len(parts) != 2 {
		log.Printf("Unsuccessfull split error: %v\n", parts)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	_, err := verifier.Verify(Ctx, parts[1])

	if err != nil {
		log.Printf("Verify error: %v\n", err)
		w.WriteHeader(http.StatusForbidden)
		return
	}

	bodyLen = DefaultBodyLean
	conLen := r.Header.Get("Content-Length")
	if conLen == "" {
		log.Printf("Content-Length is empty. Going to use DefaultBodyLean: %v\n", DefaultBodyLean)
	} else {
		bodyLen, err = strconv.Atoi(conLen)
		if err != nil {
			log.Printf("Error while converting sting into int: %v\n", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	}

	reqBody = nil
	if r.Body != nil {
		reqBody = make([]byte, bodyLen)
		n, err := r.Body.Read(reqBody)
		if err != nil {
			if err == io.EOF {
				log.Printf("EOF reached. %v bytes readed from Request Body\n", n)
			} else {
				log.Printf("Read body error: %v\n", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
		}
		reqBody = reqBody[0:n]
	} else {
		log.Println("Request Body is nil")
	}

	headers := map[string]string{
		"Content-Type": "application/json",
	}
	body, statusCode, err := HttpRequest(headers, reqBody, ElkUrl, "GET")
	if err != nil {
		log.Printf("HttpRequest: %v\n", err)
		return
	}

	w.WriteHeader(statusCode)
	_, err = w.Write(body)
}
