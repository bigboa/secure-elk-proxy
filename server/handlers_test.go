package server

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

const token = "Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJpOFV2cWd3T1d4NWl1RFp0ZFRyUldOUlpoVnB0VGxtR2Y1OXBwWGZsaDhvIn0.eyJqdGkiOiJiNmFjNTRlYi1jYWMyLTQ3N2UtYWUzOC1jNGQxOTExMmM0OTkiLCJleHAiOjE1NzM0MDY4OTQsIm5iZiI6MCwiaWF0IjoxNTczMzk5Njk0LCJpc3MiOiJodHRwOi8vNDYuMTcuNDUuMTcwOjgwODAvYXV0aC9yZWFsbXMvdW5pdmVyc2l0eS1wcm9qZWN0IiwiYXVkIjpbInVpLXNlcnZpY2UiLCJhY2NvdW50Il0sInN1YiI6ImFlOGRlOGFhLWI0ZDUtNGMxZS1hNDUwLTA5ZjExY2VkZWQxMSIsInR5cCI6IkJlYXJlciIsImF6cCI6InVpLXNlcnZpY2UiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiI2ZmY4YTU3Mi1mMzY0LTRjZWEtYmVlZi02MTczZDEyMWIwMzYiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbImh0dHA6Ly9sb2NhbGhvc3Q6ODg4OCJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiUk9MRV9VU0VSIiwib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoicHJvZmlsZSBlbWFpbCIsImFjY291bnRfaWQiOiI4NWI1MzFmOC05YzY4LTRjZDEtODQ4NS00ZTFmNjAzNWM3MTMiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsInByZWZlcnJlZF91c2VybmFtZSI6InRlc3QzNDgwNDU3MTc3NTgwNjg3OTgzdXNlciIsImVtYWlsIjoidGVzdDM0ODA0NTcxNzc1ODA2ODc5ODNAdGVzdC5ydSJ9.UuX2uSdpr4Rtwdr5e6eiQaxlPAYS2ON0vI4bzFSHhMwbJYiIebBDTnNOgLeZ4urkK-onqa9mdKPc6HHV8QvfvNWQ-R_7jKDPxCKX53R_zbPyNLz4fFxpCZjrdZmACVV1935nNKGUHF7pOzZvwh5tTqblC-rAPbNSpJgs6jRgLW8MOaPgaz2HAnNjDBaOaEBsmWgmmQ3MGOj0fayhA9gHdxE0nJMmr-U8JELDve6VpgPPG9dF8Aazu2a6oFdKR_X9tKDq0DCWu3d8YIDD0roubVBMOc9K-SdidCNWiiHdZql2kQfEe9qibCCLQLXuHgQ7LgIYVZbV5GQK4sdbBWrk9g"

func TestGetRootWithoutToken(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Root)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusUnauthorized {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusUnauthorized)
	}
}

func TestGetRootWithBadToken(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Add("Authorization", "Bad_token!!!")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Root)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}

func TestGetRootWithWrongToken(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Add("Authorization", "Bad token!!!")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Root)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusForbidden {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusForbidden)
	}
}

func TestGetRootWithWrongBody(t *testing.T) {
	req, err := http.NewRequest("GET", "/", strings.NewReader("not a json"))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Add("Authorization", token)
	req.Header.Add("Content-Length", "5")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Root)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusInternalServerError)
	}
}

func TestGetRootWithNilBody(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Add("Authorization", token)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Root)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

func TestGetRootWithWrongContentLength(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Add("Authorization", token)
	req.Header.Add("Content-Length", "ops!!!")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Root)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}
