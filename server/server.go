package server

import (
	"net/http"
)

func StartServer() error {
	http.HandleFunc("/", Root)

	return http.ListenAndServe("localhost:8181", nil)
}
